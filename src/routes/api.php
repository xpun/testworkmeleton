<?php

use App\Http\Controllers\Rate\RateController;
use App\Http\Controllers\Rate\RateConvertController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'bearer'], function () {
    Route::get('rates', RateController::class);
    Route::post('convert', RateConvertController::class);
});

