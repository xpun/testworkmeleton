<?php

namespace Tests\Feature\Rate;

use App\Jobs\Rate\RateUpdateJob;
use App\Models\Rate\Rate;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RateTest extends TestCase
{
    use WithFaker;

    const URL_RATES = '/api/v1/rates';
    const URL_CONVERT = '/api/v1/convert';

    public static function getHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . config('bearer.key')
        ];
    }

    public function testMiddleware()
    {
        $this->json('GET', self::URL_RATES)->assertForbidden();
    }

    public function testIndex()
    {
        (new RateUpdateJob())->handle();

        $currencies = collect(Rate::currencies(false));

        $url = self::URL_RATES;

        $this
            ->json('GET', $url, [], self::getHeaders())
            ->assertSuccessful()
            ->assertJsonCount($currencies->count());

        $currency = $currencies->random();

        $url .= "?filter[currency]=$currency";

        $this
            ->json('GET', $url, [], self::getHeaders())
            ->assertSuccessful()
            ->assertJsonCount(1)
            ->assertJsonStructure([$currency]);
    }

    public function testConvert()
    {
        $currencies = collect(Rate::currencies(false));

        $params = [
            'currency_from' => $currencies->random(),
            'currency_to' => Rate::CURRENCY_BTC,
            'value' => $this->faker->randomFloat()
        ];

        $this
            ->json('POST', self::URL_CONVERT, $params, self::getHeaders())
            ->assertSuccessful()
            ->assertJsonFragment($params);
    }
}
