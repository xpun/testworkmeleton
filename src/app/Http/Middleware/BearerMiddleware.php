<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BearerMiddleware
{
    const TOKEN_LENGTH = 64;

    const TOKEN_PREFIX = 'Bearer ';

    const TOKEN_PREFIX_OFFSET = 7;

    const TOKEN_REGEXP = '/^[a-zA-Z0-9_\-]*/';

    public function handle(Request $request, Closure $next)
    {
        if ($this->validToken((string)$request->header('Authorization'))) {
            return $next($request);
        }

        return response()->json([
            'status' => 'error',
            'code' => 403,
            'message' => 'Invalid token'
        ], 403);
    }

    public function validToken(string $token): bool
    {
        $prefix = substr($token, 0, self::TOKEN_PREFIX_OFFSET);

        if ($prefix !== self::TOKEN_PREFIX) {
            return false;
        }

        $token = substr($token, self::TOKEN_PREFIX_OFFSET);

        return $this->validLength($token)
            && $this->validRegexp($token)
            && $this->validKey($token);
    }

    public function validLength(string $token): bool
    {
        return strlen($token) === self::TOKEN_LENGTH;
    }

    public function validRegexp(string $token): bool
    {
        return (bool)preg_match(self::TOKEN_REGEXP, $token);
    }

    public function validKey(string $token): bool
    {
        return $token === config('bearer.key');
    }
}
