<?php

namespace App\Http\Controllers\Rate;

use App\Http\Controllers\Controller;
use App\Http\Resources\Rate\RateCollection;
use App\Services\Rate\RateService;

class RateController extends Controller
{
    public function __invoke(): RateCollection
    {
        $service = new RateService();

        $res = $service->get();

        return new RateCollection($res);
    }
}
