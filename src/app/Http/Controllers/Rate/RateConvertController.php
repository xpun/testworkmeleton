<?php

namespace App\Http\Controllers\Rate;

use App\Http\Controllers\Controller;
use App\Http\Requests\Rate\RateConvertRequest;
use App\Http\Resources\Rate\RateConvertResource;
use App\Services\Rate\RateConvertService;

class RateConvertController extends Controller
{
    public function __invoke(RateConvertRequest $request): RateConvertResource
    {
        $service = new RateConvertService($request);

        $res = $service->convert();

        return new RateConvertResource($res);
    }
}
