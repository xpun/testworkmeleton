<?php

namespace App\Http\Resources\Rate;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RateCollection extends ResourceCollection
{
    public static $wrap = '';

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $res = [];

        foreach ($this->collection as &$item){
            $res[$item->currency] = $item->amount_commission;
        }

        return $res;
    }
}
