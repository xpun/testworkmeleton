<?php

namespace App\Http\Resources\Rate;

use Illuminate\Http\Resources\Json\JsonResource;

class RateConvertResource extends JsonResource
{
    public static $wrap = '';

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->only([
            'currency_from',
            'currency_to',
            'value',
            'converted_value',
            'rate',
            'created_at'
        ]);
    }
}
