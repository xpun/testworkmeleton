<?php

namespace App\Http\Requests\Rate;

use App\Models\Rate\Rate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RateConvertRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency_from' => [
                'required',
                'string',
                Rule::in(Rate::currencies())
            ],
            'currency_to' => [
                'required',
                'string',
                Rule::in(Rate::currencies()),
                'different:currency_from'
            ],
            'value' => 'required|numeric|min:0.01',
        ];
    }
}
