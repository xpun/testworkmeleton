<?php

namespace App\Models\Rate;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $guarded = ['id'];

    const CURRENCY_BTC = 'BTC';

    public static function currencies(bool $all = true): array
    {
        $currencies = [
            'USD',
            'AUD',
            'BRL',
            'CAD',
            'CHF',
            'CLP',
            'CNY',
            'DKK',
            'EUR',
            'GBP',
            'HKD',
            'INR',
            'ISK',
            'JPY',
            'KRW',
            'NZD',
            'PLN',
            'RUB',
            'SEK',
            'SGD',
            'THB',
            'TRY',
            'TWD',
        ];

        if ($all) {
            $currencies[] = self::CURRENCY_BTC;
        }

        return $currencies;
    }
}
