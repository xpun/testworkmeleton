<?php


namespace App\Services\Rate;


use App\Models\Rate\Rate;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\QueryBuilder;

class RateService
{
    const COMMISSION = 0.98;

    public function get(): Collection
    {
        return QueryBuilder::for(Rate::class)
            ->allowedFilters('currency')
            ->oldest('amount')
            ->get();
    }

    /**
     * Update values in db
     * @return int
     */
    public function update(): int
    {
        return DB::transaction(function () {
            $rates = RateBlockchainService::get();

            $insert = [];

            $now = now();

            foreach ($rates as $key => $val) {
                $amount = $val[RateBlockchainService::RATE_KEY];

                $insert[] = [
                    'currency' => $key,
                    'amount' => $amount,
                    'amount_commission' => $this->amountCommission($amount),
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            Rate::query()->delete();

            return (int)Rate::insert($insert);
        });
    }

    public function amountCommission(float $amount): float
    {
        return $amount * self::COMMISSION;
    }
}
