<?php


namespace App\Services\Rate;


use Illuminate\Support\Facades\Http;

class RateBlockchainService
{
    const URL = 'https://blockchain.info/ticker';

    const RATE_KEY = '15m';

    public static function get(): array
    {
        return Http::get(self::URL)->json();
    }
}
