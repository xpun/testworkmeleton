<?php


namespace App\Services\Rate;


use App\Http\Requests\Rate\RateConvertRequest;
use App\Models\Rate\Rate;
use App\Models\Rate\RateConvert;

class RateConvertService
{
    public $from;

    public $to;

    public $value;

    public function __construct(RateConvertRequest $request)
    {
        $this->from = $request->currency_from;

        $this->to = $request->currency_to;

        $this->value = $request->value;
    }

    public function convert(): RateConvert
    {
        $rates = RateBlockchainService::get();

        list($rate, $convertedValue) = $this->getRateAndConvertedValue($rates);

        return RateConvert::create([
            'currency_from' => $this->from,
            'currency_to' => $this->to,
            'value' => $this->value,
            'converted_value' => $convertedValue,
            'rate' => $rate,
        ]);
    }

    public function getRateAndConvertedValue(array $rates): array
    {
        if ($this->from === Rate::CURRENCY_BTC) {
            $rate = $rates[$this->to][RateBlockchainService::RATE_KEY];

            $converted = round($this->value * $rate * RateService::COMMISSION, 2);
        } else {
            $rate = $rates[$this->from][RateBlockchainService::RATE_KEY];

            $converted = round($this->value / $rate * RateService::COMMISSION, 10);
        }

        return [$rate, $converted];
    }
}
