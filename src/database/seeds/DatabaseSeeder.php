<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(\App\Models\User::class, 10)->create();

        foreach ($users as $user) {
            factory(\App\Models\UserBook::class, 2)->create([
                'user_id' => $user->id
            ]);
        }
    }
}
