# TestWorkMeleton

### Задание 1
##### Необходимо написать SQL запрос, который найдет и выведет всех читателей, возраста от 7 и до 17 лет, которые взяли только 2 книги и все книги одного и того же автора.

```
SELECT 
    users.id AS ID,
    concat(users.first_name," ", users.last_name) Name,
    books.author,
    GROUP_CONCAT(books.name SEPARATOR ', ') Books
FROM user_books 
INNER JOIN books ON user_books.book_id = books.id 
INNER JOIN users ON user_books.user_id = users.id 
WHERE user_books.user_id IN (SELECT user_books.user_id FROM user_books GROUP BY user_books.user_id HAVING COUNT(*) = 2)
AND users.age between 7 and 12
GROUP BY user_books.user_id, books.author 
HAVING COUNT(user_books.user_id) = 2
ORDER BY user_books.user_id
```

### Задание 2
##### Middleware - App/Http/Middleware/BearerMiddleware
##### Middleware token - .env.example BEARER_KEY


##### Сервисы - app/Services/Rate
##### Контроллеры - app/Http/Controllers/Rate
##### Ресурсы - app/Http/Resources/Rate
##### Тесты - tests/Feature/Rate
###### Каждую минуту с помощью cron запускается RateUpdateJob, которая обновляет данные в таблице "rates"


