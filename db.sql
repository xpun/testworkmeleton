-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.29 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5958
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for testwork
CREATE DATABASE IF NOT EXISTS `testwork` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `testwork`;

-- Dumping structure for table testwork.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.books: ~20 rows (approximately)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` (`id`, `name`, `author`, `created_at`, `updated_at`) VALUES
	(1, 'BOOK 1', 'Prof. Brandt Ferry', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(2, 'BOOK 2', 'Prof. Brandt Ferry', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(3, 'Book 3', 'Cornell Kirlin DVM', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(4, 'Book 4', 'Cornell Kirlin DVM', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(5, 'Mrs.', 'Arthur Dicki', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(6, 'Prof.', 'Isaiah Reilly', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(7, 'Prof.', 'Ms. Cara Towne III', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(8, 'Miss', 'June Reilly', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(9, 'Ms.', 'Josue Nicolas', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(10, 'Dr.', 'Lucio Deckow', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(11, 'Mr.', 'Cameron Friesen', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(12, 'Mrs.', 'Chad Weimann', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(13, 'Ms.', 'Kathleen Fay', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(14, 'Prof.', 'Modesta Halvorson V', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(15, 'Mr.', 'Deondre Treutel I', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(16, 'Dr.', 'Mrs. Madaline Mills', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(17, 'Dr.', 'Jaqueline Homenick', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(18, 'Ms.', 'Prof. Winfield Legros PhD', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(19, 'Dr.', 'Virgie Reichel', '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(20, 'Mr.', 'Jaqueline Lakin IV', '2021-02-07 11:36:11', '2021-02-07 11:36:11');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;

-- Dumping structure for table testwork.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table testwork.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.migrations: ~6 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(3, '2021_02_06_185605_create_books_table', 1),
	(4, '2021_02_06_185632_create_user_books_table', 1),
	(5, '2021_02_07_085733_create_rates_table', 1),
	(6, '2021_02_07_091936_create_rate_converts_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table testwork.rates
CREATE TABLE IF NOT EXISTS `rates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(65,10) unsigned NOT NULL,
  `amount_commission` decimal(65,10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.rates: ~0 rows (approximately)
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;

-- Dumping structure for table testwork.rate_converts
CREATE TABLE IF NOT EXISTS `rate_converts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `currency_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(65,10) unsigned NOT NULL,
  `converted_value` decimal(65,10) unsigned NOT NULL,
  `rate` decimal(65,10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.rate_converts: ~0 rows (approximately)
/*!40000 ALTER TABLE `rate_converts` DISABLE KEYS */;
/*!40000 ALTER TABLE `rate_converts` ENABLE KEYS */;

-- Dumping structure for table testwork.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_age_index` (`age`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.users: ~10 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `age`, `created_at`, `updated_at`) VALUES
	(1, 'Alex', 'Harvey', 7, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(2, 'Bob', 'Hartmann', 12, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(3, 'Daisy Hirthe', 'Marquardt', 21, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(4, 'Winston Hyatt DVM', 'Simonis', 38, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(5, 'Ernestine Ratke PhD', 'Smith', 29, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(6, 'Berenice Feil', 'Anderson', 66, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(7, 'Antwon Tromp', 'Mayert', 19, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(8, 'Macey Watsica', 'Purdy', 8, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(9, 'Prof. Adolf Cummings DDS', 'Fritsch', 15, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(10, 'Mr. Trey Bayer', 'Bartoletti', 20, '2021-02-07 11:36:11', '2021-02-07 11:36:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table testwork.user_books
CREATE TABLE IF NOT EXISTS `user_books` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `book_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_books_user_id_foreign` (`user_id`),
  KEY `user_books_book_id_foreign` (`book_id`),
  CONSTRAINT `user_books_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  CONSTRAINT `user_books_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table testwork.user_books: ~20 rows (approximately)
/*!40000 ALTER TABLE `user_books` DISABLE KEYS */;
INSERT INTO `user_books` (`id`, `user_id`, `book_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(2, 1, 1, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(3, 2, 3, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(4, 2, 4, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(5, 3, 3, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(6, 3, 6, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(7, 4, 7, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(8, 4, 8, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(9, 5, 9, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(10, 5, 10, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(11, 6, 11, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(12, 6, 12, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(13, 7, 13, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(14, 7, 14, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(15, 8, 15, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(16, 8, 16, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(17, 9, 17, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(18, 9, 18, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(19, 10, 19, '2021-02-07 11:36:11', '2021-02-07 11:36:11'),
	(20, 10, 20, '2021-02-07 11:36:11', '2021-02-07 11:36:11');
/*!40000 ALTER TABLE `user_books` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
